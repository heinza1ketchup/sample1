// Copyright Epic Games, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameModeBase.h"
#include "UE4_VR_DemoGameModeBase.generated.h"

/**
 * 
 */
UCLASS()
class UE4_VR_DEMO_API AUE4_VR_DemoGameModeBase : public AGameModeBase
{
	GENERATED_BODY()
	
};
